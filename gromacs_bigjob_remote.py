import os,sys,time,random
import saga
import traceback
from pilot import PilotComputeService, ComputeDataService, State

#REDIS_PWD = 

COORD = "redis://%s@gw68.quarry.iu.teragrid.org:6379"%REDIS_PWD

HOSTNAME = "vivek91@stampede.tacc.xsede.org"

WORKDIR = "/home1/02734/vivek91/saga_gromacs/output/"

NUMBER_JOBS = 1  

if __name__ == "__main__":
    pilot_compute_service = PilotComputeService(COORD)
    #describe pilot
    pilot_compute_description = {
                            "service_url" :"slurm+ssh://%s" %HOSTNAME,
                            "number_of_processes" : 12, 
                            "queue" : 'development',
                            "spmd_variation" : '12way',
                            "working_directory" : WORKDIR,
                            "walltime": 10
    }
    #create pilot
    pilotjob = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description)
    pilot_toc = time.time()
    compute_data_service=ComputeDataService()
    compute_data_service.add_pilot_compute_service(pilot_compute_service)
    print 'Finished pilot job creation'
    print 'Start CU submission'
    
    i=0
    job_tic = time.time()
    while(i<NUMBER_JOBS):
	print 'Submitting job',(i+1)
        compute_unit_description= {
        				"executable" : "/bin/bash",
        				"arguments" : ["-c","\"module load gromacs && pdb2gmx -f  -ff \" amber03 \" -water none \""], 
        				"number_of_processes" : 1, 
        				"output":"stdout.txt",
        				"error":"stderr.txt"
        			}
        compute_unit =compute_data_service.submit_compute_unit(compute_unit_description)
        i+=1

    compute_data_service.wait()
    compute_data_service.cancel()
    pilot_compute_service.cancel()

