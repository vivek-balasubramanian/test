import sys
import saga
import os

remote_host = 'vivek91@stampede.tacc.xsede.org'

remote_directory = '/home1/02734/vivek91/saga_gromacs/'


if __name__ == "__main__":
    try:
	
        #Create job service
        js = saga.job.Service('slurm+ssh://%s'%(remote_host))
        
        #Create job description
        jd = saga.job.Description()
        
	jd.working_directory = '%s'%(remote_directory)
        jd.executable = '/bin/bash'
        jd.arguments = ["-c","\"module load gromacs && pdb2gmx -f  -ff \" amber03 \" -water none \"" ]
        jd.total_cpu_count = 12
        jd.wall_time_limit = 10
        jd.queue = 'development'
        jd.spmd_variation ='12way'
        jd.output = 'job1.stdout'
        jd.error = 'job1.stderr'
        
        job1 = js.create_job(jd)
        
        # Check our job's id and state
        print "Job ID    : %s" % (job1.id)
        print "Job State : %s" % (job1.state)

        print "\n...starting job...\n"

        # Now we can start our job.
        job1.run()

        print "Job ID    : %s" % (job1.id)
        print "Job State : %s" % (job1.state)

        print "\n...waiting for job...\n"
        # wait for the job to either finish or fail
        job1.wait()

        print "Job State : %s" % (job1.state)
        print "Exitcode  : %s" % (job1.exit_code)
        
    except saga.SagaException, ex:
        # Catch all saga exceptions
        print "An exception occured: (%s) %s " % (ex.type, (str(ex)))
        # Trace back the exception. That can be helpful for debugging.
        print " \n*** Backtrace:\n %s" % ex.traceback
#        return -1

